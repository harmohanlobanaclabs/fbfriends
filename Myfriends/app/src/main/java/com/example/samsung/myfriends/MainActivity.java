package com.example.samsung.myfriends;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.facebook.Session;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import entities.Friend;
import util.FacebookUtils;


public class MainActivity extends ActionBarActivity {
    List<Friend> listFriend;
    ListView friendList;
    ArrayAdapter<Friend> Adapters;
    Intent friendDetail;
    String accessToken, link, gender, lastName, friendId, firstName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listFriend = new ArrayList<Friend>();
        friendList = (ListView) findViewById(R.id.list);
        FacebookUtils.fbLogin(0, MainActivity.this);
        //  final List<String> list = new ArrayList<String>();
        //    adapter=new com.example.samsung.Json.Adapter(list, MainActivity.this);
        //   adapter=new Myadapter(list,MainActivity.this);
        //   list_view.setAdapter(adapter);
        Adapters = new ArrayAdapter<Friend>(this, R.layout.row, listFriend);
        friendList.setAdapter(Adapters);
// a;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(this, requestCode,
                resultCode, data);
    }


    public void onClick(View view) {
        friendList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                friendId = listFriend.get(position).friendId;


                String uri = "https://graph.facebook.com/" + friendId + "?access_token=" + accessToken;
                final AsyncHttpClient friendClient = new AsyncHttpClient();

                friendClient.get(MainActivity.this, uri, new AsyncHttpResponseHandler() {

                    @Override
                    public void onSuccess(String details) {
                        super.onSuccess(details);

                        try {

                            JSONObject Details = new JSONObject(details);
                            firstName = Details.getString("first_name");
                            lastName = Details.getString("last_name");
                            gender = Details.getString("gender");
                            link = Details.getString("link");
                            friendDetail = new Intent(MainActivity.this, MainActivity2.class);
                            friendDetail.putExtra("firstName", firstName);
                            friendDetail.putExtra("lastName", lastName);
                            friendDetail.putExtra("gender", gender);
                            friendDetail.putExtra("link", link);
                            startActivity(friendDetail);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable, String s) {
                        super.onFailure(throwable, s);

                        Toast.makeText(MainActivity.this, "FAILURE", Toast.LENGTH_SHORT).show();
                    }
                });


            }
        });
    }

    public void getFriends(String fbAccessToken) {
        accessToken = fbAccessToken;
        String url = "https://graph.facebook.com/me/friends?access_token=" + fbAccessToken;
        final AsyncHttpClient friendClient = new AsyncHttpClient();
        friendClient.get(MainActivity.this, url, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(String s) {
                super.onSuccess(s);
                listFriend.clear();
                try {
                    JSONObject friendNameList = new JSONObject(s);
                    JSONArray data = friendNameList.optJSONArray("data");
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jsonobject = data.getJSONObject(i);
                        String name =jsonobject.getString("name");
                        friendId = jsonobject.getString("id");
                        listFriend.add(new Friend(friendId, name));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Adapters.notifyDataSetChanged();
            }

                /*
                  String id_selected = friend_id.get(position);
                    String friend_uri = uriProfile + id_selected + "?access_token=" + token;
                    final AsyncHttpClient client = new AsyncHttpClient();
                    client.get(MainActivity.this, friend_uri, new AsyncHttpResponseHandler() {
                 */


            @Override
            public void onFailure(Throwable throwable, String s) {
                super.onFailure(throwable, s);
                Toast.makeText(MainActivity.this, "FAILURE", Toast.LENGTH_LONG).show();
            }
        });

    }


}

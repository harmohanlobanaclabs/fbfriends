package entities;

/**
 * Created by samsung on 2/4/2015.
 */
public class Friend {
    public String name;
    public String friendId;

    public Friend(String friendId, String name) {
        this.friendId = friendId;
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;

    }
}

